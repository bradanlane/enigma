/* ************************************************************************************
* File:    enigma.h
* Date:    2021.09.12
* Author:  Bradan Lane Studio
*
* This content may be redistributed and/or modified as outlined under the MIT License
*
* ************************************************************************************/

/* ---

# Enigma Encryption and Configuration API

The origins of this code flow back to [munnellg EnigmaMachine](https://github.com/munnellg/EnigmaMachine) project under the MIT license.

The original author stated, _"If you're planning on using this in a project of your own, bear in mind that it has had very little testing. It's a quick little hack I threw together to pass the time on a rainy weekend."_

The following API has been refactored, filled out with Get/Set functions, and extended to support actual enigma machine tasks beyond basic encrypt/decript of characters.

**Note:** The API documentation order has been chosen to avoid as much forward referencing as possible.
While this is less intuitive for the reader, it is more conducive to the maintenance of the code. _it is what it is_

--------------------------------------------------------------------------
--- */


/* ---
## Enigma Machine

"The Enigma machine is a cipher device developed and used in the early- to mid-20th century to protect commercial, diplomatic, and military communication. It was employed extensively by Nazi Germany during World War II, in all branches of the German military." - [WIKIPEDIA](https://en.wikipedia.org/wiki/Enigma_machine)

There has been plenty of descriptions of the Enigma machine. It was not just an encryption device used by the Germans during WWII.
However, the usecase driving the development and test of this API is to support the implementation of a digital facsimile if the military
hardware. Civilian devices may be implemented using this API provided the desired machine used rotors which were used by one of the
machine types implemented within the API.

If you are interesting is experiencing an Enigma machine, consider the [Enigma Simulator](http://users.telenet.be/d.rijmenants/en/enigmasim.htm)
which provides an accurate visual and functional representation as well as considerable documentation and tutorials.


### Machine

The German military Enigma machines had either 3 or 4 positions and the machines came with either a set of 5 rotors, 8 rotors, or 8+2 rotors.

There are three machine types supported by this project.

|Machine|Rotor<br>positions|Available<br>Rotors|Reflector|Plug Wires|
|:-----|:-----:|:-----:|:-----:|:-----:|
|Wehrmacht|3|5|B or C|10|
|Kriegsmarine M3|3|8|B or C|10|
|Kriegsmarine M4|4|8<br>Beta<br>Gamma|Thin<br>B or C|10|

While the API provides configuration for the machine type, the physical machine _is what it is_ and does not change.


### Reflector

A physical Enigma machine has a pre-stalled reflector disk which has internal mapping wires and _reflects_ the electrical signal back through the rotors.

While the API provides configuration for the machine type and the reflector, the physical machine _is what it is_ and does not change.

### Rotor

A rotor from a real Enigma machine has a specifc number stamped on it.
Rotors were indicated with Roman numerals.
It has thumb wheel on one side and a wide rotating ring on the other.
The ring is help in place by a spring clip. Pressing the spring clip allows the ring to rotate independent of the rest of the rotor.
The ring has numbers on the side and letters around the perimeter.

When a rotor is held vertically, the letters are visible.
When a rotor is help face on with the thumb wheel at the back, the ring numbers are visible.


### Plugboard

The lower front of a military Enigma machine consisted of 26 plugs.
The machine was provided with 10 wires with compatible plugs on each end.
The plug wires perform letter substitution.
For example, assumed there is a wire between plug 'A' and 'R' and another between 'H' and 'B'.
When a user hit the 'R' key it would behave like an 'A' at the beginning of the encrypting sequenced.
If the end of the encrypting sequence resulted in a 'B' it would light up the 'H' bulb as the final step.
The wires were reciprocal, meaning 'R' becomes 'A' and 'A' become 'R'. Likewise, 'B' becomes 'H' and 'H' becomes 'B'.

### Operations

The German Enigma machines has multiple settings. The procedure for determining the settings varied for different services and at different times during WWII.

As a generalization, the 3 or 4 rotors were selected from the available set.
Each rotor had its ring positions set. Then the rotors were inserted into their designated position.
Additionally, plugboard wires were installed.

Daily settings sheets were used to determine which rotors to use,
what ring settings to use, what position order for the rotors, and what letters of the plugboard were populated.

The operator also needed to choose a code key. Depending on what procedure was used, the key was determined - in part - from the daily sheet or a code book.

--- */

/* ---
## API Definitions and Enumerations
--- */

#ifndef __ENIGMA_API_H
#define __ENIGMA_API_H


#ifndef __RANDOM_H_
#warning missing suitable implementation of randomCharGet()
#define randomCharGet() ('A')
#endif

/* ---
```C
*/
// operational modes
enum { MACHINE_W3 = 0,
	   MACHINE_M3,
	   MACHINE_M4
};
/*
```
--- */
#define MACHINES_SIZE 3

char *machine_names[MACHINES_SIZE] = {"Wehrmacht #", "Kriegsmarine M3 #", "Kriegsmarine M4 #"};

/* ---
```C
*/
// encryption rotors to be placed in the rack.
// ROTOR_BETA and ROTOR_GAMMA can only go in the
// fourth rack position and are only used with MACHINE_M4
enum { ROTOR_I = 0,
	   ROTOR_II,
	   ROTOR_III,
	   ROTOR_IV,
	   ROTOR_V,
	   ROTOR_VI,
	   ROTOR_VII,
	   ROTOR_VIII,
	   ROTOR_BETA,
	   ROTOR_GAMMA };
/*
```
--- */

const char *rotor_names[] = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "B", "G"};

/* ---
```C
*/
// The position within the rotor set is numerically ordered from right to left
enum { ROTOR_RIGHT = 0,
	   ROTOR_MIDDLE,
	   ROTOR_LEFT,
	   ROTOR_EXTRA
};
/*
```
--- */

/* ---
```C
*/
// reflectors which sit at the end of the rack and bounce signal back through rotors.
// A physical MACHINE_M4 Enigma machine uses thin reflectors. Each reflector has a different internal mapping.
enum { REFLECTOR_B = 0,
	   REFLECTOR_C,
	   REFLECTOR_B_THIN,
	   REFLECTOR_C_THIN
};
/*
```
--- */

#define ENIGMA_RACK_SIZE	4
#define ENIGMA_KEY_SIZE		5 // this is the max visible key; only RACK_SIZE will be used for configuring the machine
#define ENIGMA_MAP_SIZE		26

#define MAX_NOTCHES 2

#define INDEX2CHAR(x) ((x) + 'A')					   // index to letter
#define CHAR2INDEX(x) ((x) - 'A')					   // letter to index
#define IS_VALID(x)	  (((x) >= 0) && ((x) < ENIGMA_MAP_SIZE)) // check if x can be encrypted

struct encrypter {
	int initial, rotation, ringset, num_notches, code;
	int notches[MAX_NOTCHES];
	char map[ENIGMA_MAP_SIZE+1]; // input to output map (for forward pass)
	char inv[ENIGMA_MAP_SIZE+1]; // output to input maps (for backward pass); this wastes 26*5=130 bytes since reflectors and plugboard do not have inv
};

static struct encrypter rotors[] = {
	// Rotor I
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 1, .notches = {16}, .code = ROTOR_I, .map = "EKMFLGDQVZNTOWYHXUSPAIBRCJ", .inv = "UWYGADFPVZBECKMTHXSLRINQOJ"},
	// Rotor II
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 1, .notches = {4}, .code = ROTOR_II, .map = "AJDKSIRUXBLHWTMCQGZNPYFVOE", .inv = "AJPCZWRLFBDKOTYUQGENHXMIVS"},
	// Rotor III
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 1, .notches = {21}, .code = ROTOR_III, .map = "BDFHJLCPRTXVZNYEIWGAKMUSQO", .inv = "TAGBPCSDQEUFVNZHYIXJWLRKOM"},
	// Rotor IV
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 1, .notches = {9}, .code = ROTOR_IV, .map = "ESOVPZJAYQUIRHXLNFTGKDCMWB", .inv = "HZWVARTNLGUPXQCEJMBSKDYOIF"},
	// Rotor V
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 1, .notches = {25}, .code = ROTOR_V, .map = "VZBRGITYUPSDNHLXAWMJQOFECK", .inv = "QCYLXWENFTZOSMVJUDKGIARPHB"},
	// Rotor VI
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 2, .notches = {25, 12}, .code = ROTOR_VI, .map = "JPGVOUMFYQBENHZRDKASXLICTW", .inv = "SKXQLHCNWARVGMEBJPTYFDZUIO"},
	// Rotor VII
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 2, .notches = {25, 12}, .code = ROTOR_VII, .map = "NZJHGRCXMYSWBOUFAIVLPEKQDT", .inv = "QMGYVPEDRCWTIANUXFKZOSLHJB"},
	// Rotor VIII
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 2, .notches = {25, 12}, .code = ROTOR_VIII, .map = "FKQHTLXOCBJSPDZRAMEWNIUYGV", .inv = "QJINSAYDVKBFRUHMCPLEWZTGXO"},
	// Rotor b (beta)
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 0, .code = ROTOR_BETA, .map = "LEYJVCNIXWPBQMDRTAKZGFUHOS", .inv = "RLFOBVUXHDSANGYKMPZQWEJICT"},
	// Rotor g (gamma)
	{.initial = 0, .rotation = 0, .ringset = 0, .num_notches = 0, .code = ROTOR_GAMMA, .map = "FSOKANUERHMBTIYCWLQPZXVGJD", .inv = "ELPZHAXJNYDRKFCTSIBMGWQVOU"}};

// On the physical machine, the reflector would sit at the end of the rack and
// **reflect** the electrical impulse representing the CHAR2INDEX'd character back through
// the series of rotors in the rack.
static struct encrypter reflectors[] = {
	{.map = "YRUHQSLDPXNGOKMIEBFZCWVJAT", .code = REFLECTOR_B},		 // M3 B
	{.map = "FVPJIAOYEDRZXWGCTKUQSBNMHL", .code = REFLECTOR_C},		 // M3 C
	{.map = "ENKQAUYWJICOPBLMDXZVFTHRGS", .code = REFLECTOR_B_THIN}, // M4 B thin
	{.map = "RDOBJNTKVEHMLFCWZAXGYIPSUQ", .code = REFLECTOR_C_THIN}, // M4 C thin
};

// The plugboard was used by the enigma operator to specifically wire an input/output
// to a different value before and after it hit the rack. By default every character in
// the plugboard maps to itself.
static struct encrypter plugboard = {.map = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"};

// Rotor configurations in the rack. Note that the rotor in the fourth position is only
// used if we are in MACHINE_M4
// to simplify code, the rack positioned (0..3) are opposite the visual L-R positions; eg the right most rack position is 0

static struct encrypter rack[ENIGMA_RACK_SIZE]; // Sequence of rotors to be used for encryption
static struct encrypter reflector;		 // Index of chosen reflector
static int machine_type = MACHINE_W3;	 // Type of Enigma machine being emulated
static int num_rotors = 3;				 // Number of rotors in the rack (mode dependent)
static int num_options = 5;				 // Number of available rotors in the box mode dependent)
static char rotation_chars[ENIGMA_RACK_SIZE+1];	 // current character displayed in rotor window
static uint8_t block_size;				// tradionally messages were rendered into 4 or 5 character blocks

static bool _enigma_data_changed;

// the daily key (and optionally the message key) is set by the user
// the message key is used to encode the message
// the daily key is used to encode the message key
// the daily key and the encoded message key are transmitted
// they are used on the receive end to recreate the message key
static char _daily_key[ENIGMA_KEY_SIZE + 1], _message_key[ENIGMA_KEY_SIZE + 1];

static bool _using_random_key;	// allows the program to decide if it should automatically generate a new key

static int offset(struct encrypter *e, int c) {
	c = (ENIGMA_MAP_SIZE + (c - e->ringset)) % ENIGMA_MAP_SIZE; // ringset offset - negative modulus
	return (c + e->rotation) % ENIGMA_MAP_SIZE;		  // rotation offset
}

static int undo_offset(struct encrypter *e, int c) {
	c = (ENIGMA_MAP_SIZE + (c - e->rotation)) % ENIGMA_MAP_SIZE; // undo rotation offset - neg modulo
	return (c + e->ringset) % ENIGMA_MAP_SIZE;			   // undo ringset offset
}

static int inv_crypt(struct encrypter *e, int c) {
	c = offset(e, c);		   // offset for rotation and ringset
	c = CHAR2INDEX(e->inv[c]); // crypt
	return undo_offset(e, c);  // undo offset for rotation and ringset
}

static int crypt(struct encrypter *e, int c) {
	c = offset(e, c);		   // offset for rotation and ringset
	c = CHAR2INDEX(e->map[c]); // crypt
	return undo_offset(e, c);  // undo offset for rotation and ringset
}

static int is_notched(struct encrypter *e) {
	for (int i = 0; i < e->num_notches; i++)
		if (e->notches[i] == e->rotation)
			return 1;

	return 0;
}

bool enigmaChanged() {
	return _enigma_data_changed;
}

void enigmaChangedClear() {
	_enigma_data_changed = false;
}


// ================================================================================================
// PUBLIC FUNCTIONS
// ================================================================================================

/* ---
## Operational Functions

**Encode / Decode**: The primary operations used by a program will be the `enigmaEncode()` and `EnigmaEncodeString()`
functions.

--- */

/*
#### void enigmaRotorsSpin()

Advance the active rotor set one position

This function uses the rotor settings and the plugboard.

_This is rarely used outside of the API's internal functions.
It is automatically called (internally) by the `enigmaEncode()` function._
*/

static void enigmaRotorsSpin(void) {
	// check if rotor 2 is in the notch position
	if (is_notched(&rack[1])) {
		// if so, rotate rotors 2 and 3 forwards, implementing double stepping
		rack[1].rotation = (rack[1].rotation + 1) % ENIGMA_MAP_SIZE;
		rack[2].rotation = (rack[2].rotation + 1) % ENIGMA_MAP_SIZE;
		rotation_chars[1] = 'A' + rack[1].rotation;
		rotation_chars[2] = 'A' + rack[2].rotation;
	}

	// if rotor 1 is in the notched position, rotate rotor 2 forwards
	if (is_notched(&rack[0])) {
		rack[1].rotation = (rack[1].rotation + 1) % ENIGMA_MAP_SIZE;
		rotation_chars[1] = 'A' + rack[1].rotation;
	}

	// rotor 1 always rotates
	rack[0].rotation = (rack[0].rotation + 1) % ENIGMA_MAP_SIZE;
	rotation_chars[0] = 'A' + rack[0].rotation;
}


/* ---
#### void enigmaRotorsBackspin()

Reverse the active rotor set one position

The active rotor set is automatically advanced during the `enigmaEncode()` process.
The only reason to use this function is as part of a user interface which supports
a `DEL` key to delete the most recent character(s).

_Implementing a general purpose editor with cursor navigation and simultaneous encoded would be very complex._

--- */

void enigmaRotorsBackspin() {
	// rotor 1 always rotates
	if ((--(rack[0].rotation)) < 0) rack[0].rotation = (ENIGMA_MAP_SIZE - 1);
	rotation_chars[0] = 'A' + rack[0].rotation;

	// if rotor 1 is in the notched position, rotate rotor 2 forwards
	if (is_notched(&rack[0])) {
		if ((--(rack[1].rotation)) < 0) rack[1].rotation = (ENIGMA_MAP_SIZE - 1);
		rotation_chars[1] = 'A' + rack[1].rotation;
	}

	// check if rotor 2 is in the notch position
	if (is_notched(&rack[1])) {
		// if so, rotate rotors 2 and 3 forwards, implementing double stepping
		if ((--(rack[2].rotation)) < 0) rack[2].rotation = (ENIGMA_MAP_SIZE - 1);
		if ((--(rack[1].rotation)) < 0) rack[1].rotation = (ENIGMA_MAP_SIZE - 1);
		rotation_chars[2] = 'A' + rack[2].rotation;
		rotation_chars[1] = 'A' + rack[1].rotation;
	}
}

/* ---
#### char enigmaEncode(char letter)

Return the encrypted letter for the input letter.

This function will advance the active rotor set one position.

--- */

char enigmaEncode(char c) {
	int i;
	char ec = CHAR2INDEX(UPPERCASE(c));

	if (IS_VALID(ec)) {
		// first, rotors spin on key press
		enigmaRotorsSpin();

		// letter passes through plugboard
		ec = crypt(&plugboard, ec);
		// then through each of the rotors in turn
		for (i = 0; i < num_rotors; i++)
			ec = crypt(&rack[i], ec);
		// bounce through the reflector
		ec = crypt(&reflector, ec);
		// back through the rotors in the opposite direction
		for (--i; i >= 0; i--)
			ec = inv_crypt(&rack[i], ec);
		// lastly, through the plugboard again
		ec = crypt(&plugboard, ec);
		// get the encrypted letter as a char
		c = INDEX2CHAR(ec);
	}

	return c; // return encrypted letter or original symbol if not valid for encryption
}


/* ---
#### uint8_t enigmaEncodeString(char* input, char* output, uint8_t size)

Encode the string and return the number of characters encoded.

This function will only encrypt ASCII letters. Letters are converted to upper case. non-letters are ignored.

This function uses the current value from `enigmaKeySizeGet()` to determine the blocksize.

The output is padded with characters to a full blocksize.
If padding will exceed the `output` buffer `size`, then the `output` is truncated to the last full blocksize.

This function will set the rotors to their start position before performing the encoding process.
Encoding will advance the active rotor set for each character encoded. The rotor set are not advanced for spaces.

--- */

uint8_t enigmaKeySizeGet();		// forward declaration
void enigmaKeyMessageRestart();	// forward declaration

uint8_t enigmaEncodeString(char* input, char *output, uint8_t size) {
	enigmaKeyMessageRestart();

	uint8_t blocksize = enigmaKeySizeGet();
	uint8_t count = 0;
	uint8_t block_chars = 0;
	uint8_t block_count = 0;
	char c;


	memset(output, 0, size);

	while ((c = *input++) && (count < (size - 1))) {
		// we discard digits, spaces, and special characters; we also uppercase everything
		if (ISLETTER(c)) {
			c = UPPERCASE(c);
			c = enigmaEncode(c);
			output[count] = c;
			count++;
			block_chars++;
			if (block_chars == blocksize) {
				block_count++;
				block_chars = 0;
				output[count] = ' ';
				count++;
			}
		}
	}

	// remove a trailing space if we added one
	if (count && output[count-1] == ' ') {
		count--;
		output[count] = 0;
	}

	if (block_chars) {
		uint8_t padding = blocksize - block_chars;
		if (size > (count + padding)) {
			// we have space fo the padding
			for (uint8_t i = 0; i < padding; i++) {
				// we could use 'X' but this is a little bit more secure
				c = enigmaEncode('A' + (count % 26));
				output[count] = c;
				count++;
			}
		} else {
			// we don't have space fo the padding; we clip the message
			uint8_t clip = blocksize - padding;
			for (uint8_t i = 0; i < clip; i++) {
				count--;
				output[count] = 0;
			}
		}
	}
	//printDevicePrintf(PRINT_UART, "(%d)[%s]\n", block_count, output);

	return block_count;
}


// ------------------------------------------------------------------------------------------------
// CONFIGURATION FUNCTIONS
// ------------------------------------------------------------------------------------------------

/* ---
## Rotor Configuration Functions

The historical process of setting up the rotors for the Enigma machine was to
- identify the rotor set _(rotors to use from all available rotors)_
- order the rotor set
- adjust the ring position for each rotor
- insert the rotor set into the machine

The plugboard may be setup before or after the rotors.

_Whereas the full rotor set was inserted in an Enigma machine, the API loads one rotor at a time into the machine._

--- */


// a couple of forward declarations to clean up the documentation order a bit
static void _enigma_rotor_set_starting_num(int position, int rotation);	// forward declaration
void enigmaRotorRingSet(int position, int ringset);						// forward declaration
uint8_t enigmaRotorRingGet(uint8_t position);							// forward declaration

enum {
	ROTOR_SET_FORCE = 0, // set the rotor as specified regardless of conflicts
	ROTOR_SET_AVAILABLE, // set the rotor to the specified / next available position to avoid conflicts
	ROTOR_SET_ADJUST	 // set the rotor as specified and adjust any conflicting rotor
};

static void enigma_set_rotor(int position, int rotor, uint8_t method) {
	if (position >= ENIGMA_RACK_SIZE) return;
	_enigma_data_changed = true;

	// IMPORTANT: position is zero-based; rotor value is zero based

	// allow the BETA and GAMMA rotors only in the last position
	if (position == (ENIGMA_RACK_SIZE - 1)) {
		// wrap around on rotors
		if (rotor < ROTOR_BETA) rotor = ROTOR_GAMMA;
		if (rotor > ROTOR_GAMMA) rotor = ROTOR_BETA;

		if (rack[position].code == rotors[rotor].code)
			return;
		rack[position] = rotors[rotor];
		// reset ring and position
		enigmaRotorRingSet(position, 1);
		_enigma_rotor_set_starting_num(position, 1);
		return;
	}

	// wrap around on rotors
	if (rotor < ROTOR_I) {
		if (machine_type == MACHINE_W3)
			rotor = ROTOR_V;
		else
			rotor = ROTOR_VIII;
	}

	if (machine_type == MACHINE_W3) {
		if (rotor > ROTOR_V) rotor = ROTOR_I;
	} else {
		if (rotor > ROTOR_VIII) rotor = ROTOR_I;
	}

	if (rack[position].code == rotors[rotor].code)
		return;

	uint8_t save_ring = enigmaRotorRingGet(position);

	switch (method) {
		case ROTOR_SET_FORCE: {
			// set it and accept the consequences
			rack[position] = rotors[rotor];
			// reset ring and position
			enigmaRotorRingSet(position, save_ring);
			_enigma_rotor_set_starting_num(position, 1);
		} break;
		case ROTOR_SET_AVAILABLE: {
			// deal with any attempt to load a rotor that is already in use

			for (uint8_t i = 0; i < 3;) { // we are only working on the 3 general purpose positions
				// if someone already has this rotor, then we increment and try again
				if (rack[i].code == rotors[rotor].code) {
					rotor++;
					i = 0;
					if (machine_type == MACHINE_W3) {
						if (rotor > ROTOR_V) rotor = ROTOR_I;
					} else {
						if (rotor > ROTOR_VIII) rotor = ROTOR_I;
					}
				}
				else
					i++;
			}
			rack[position] = rotors[rotor];
			// reset ring and position
			enigmaRotorRingSet(position, save_ring);
			_enigma_rotor_set_starting_num(position, 1);
			return; // we only ever need to reposition one rotor
		}
		case ROTOR_SET_ADJUST: {
			// set it and then scan for a conflict and change that rotor
			rack[position] = rotors[rotor];
			// reset ring and position
			enigmaRotorRingSet(position, save_ring);
			_enigma_rotor_set_starting_num(position, 1);

			// deal with any attempt to load a rotor that is already in use

			for (uint8_t i = 0; i < 3; i++) { // we are only working on the 3 general purpose positions
				// dont mess with the active position
				if (i == position) continue;
				// if there is a rack position using the rotor we just loaded,
				// then we need to scan the positions for the first unused option
				if (rack[i].code == rotor) {
					for (uint8_t j = 0; j < num_options; j++) {
						bool found = false;
						for (uint8_t k = 0; k < 3; k++) {
							if (rack[k].code == j) {
								found = true;
								break;
							}
						}
						// if we did not find a match, then we have an unused option
						if (!found) {
							save_ring = rack[i].ringset;
							rack[i] = rotors[j];
							// reset ring and position
							enigmaRotorRingSet(i, save_ring);
							_enigma_rotor_set_starting_num(i, 1);
							return; // we only ever need to reposition one rotor
						}
					}
				}
			}
		}
	}
}

/* ---
#### void enigmaRotorLoad(int position, uint8_t rotor)

Set the rotor position to the specific rotor.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The rotor is one-based. To avoid confusion, it is recommended to use _(or at least reference)_ the enumerated rotor indentifiers.

**WARNING:** This function does not test for collisions. Use `enigmaRotorLoadAdjust()` or `enigmaRotorLoadAdjust()` to automatically avoid conflicts.

--- */

void enigmaRotorLoad(int position, int rotor) {
	enigma_set_rotor(position, rotor, ROTOR_SET_FORCE);
}

/* ---
#### void enigmaRotorLoadAdjust(int position, uint8_t rotor)

Set the rotor position to the specific rotor. Adjust other rotors to avoid conflicts.
If setting the current rotor will conflict with another rotor, then the conflicting rotor is changed to resolve the conflict.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The rotor is one-based. To avoid confusion, it is recommended to use _(or at least reference)_ the enumerated rotor indentifiers.

--- */

void enigmaRotorLoadAdjust(int position, int rotor) {
	enigma_set_rotor(position, rotor, ROTOR_SET_ADJUST);
}

/* ---
#### void enigmaRotorLoadAvailable(int position, uint8_t rotor)

Attempt to set the rotor position to the specific rotor.
If this will cause a conflict, then change the specified rotor to an unused rotor.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The rotor is one-based. To avoid confusion, it is recommended to use _(or at least reference)_ the enumerated rotor indentifiers.

--- */

void enigmaRotorLoadAvailable(int position, int rotor) {
	enigma_set_rotor(position, rotor, ROTOR_SET_AVAILABLE);
}

/* ---
#### uint8_t enigmaRotorNumGet(int position)

Return the enumerated value of the rotor in the specific position.
_(refer to the [enumerations](#api-definitions-and-enumerations) near the start of this documentation)_

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).

--- */

uint8_t enigmaRotorNumGet(uint8_t position) {
	if (position >= num_rotors) return 0;
	return rack[position].code;
}

/* ---
#### char* enigmaRotorNameGet(int position)

Return string representing the _Roman numeral_ ror the rotor in the specific position.
Rotors were indicated with .

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).

_This is a convenience function for developing user interfaces._

--- */

const char *enigmaRotorNameGet(uint8_t position) {
	if (position >= num_rotors) return "";
	return rotor_names[rack[position].code];
}

/* ---
#### void enigmaRotorRingSet(int position, uint8_t value)

Adjust the ring value of rotor in the specific position.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The ring value is one-based (1 .. 26).

--- */

void enigmaRotorRingSet(int position, int ringset) {
	if (position >= num_rotors) return;
	_enigma_data_changed = true;

	// wrap around on map
	if (ringset < 1)
		ringset = ENIGMA_MAP_SIZE;

	// user values are 1..26 so we need to switch to 0..25
	if (ringset > 0) ringset--;
	ringset = (ENIGMA_MAP_SIZE + ringset % ENIGMA_MAP_SIZE) % ENIGMA_MAP_SIZE;
	rack[position].ringset = ringset;
}

/* ---
#### uint8_t enigmaRotorRingGet(int position)

Return the current ring value of rotor in the specific position.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The ring value is one-based (1 .. 26).

--- */
uint8_t enigmaRotorRingGet(uint8_t position) {
	if (position >= num_rotors) return 0;
	return rack[position].ringset + 1;
}


/* ---
#### void enigmaPlugboardMap(char a, char b)

Returns the number of plug wires currently applied

The upper limit is 13.

_Original machines had up to 10 wires because there is little improvement in complexity with 11 and beyond 11 the complexity actually goes down._

--- */
uint8_t enigmaPlugboardCount() {
	uint8_t count = 0;
	// for every plug wire, there are two characters swapped
	// we count number of characters out of place, then divide by 2
	for (uint8_t i = 0; i < ENIGMA_MAP_SIZE; i++)
		if (plugboard.map[i] != 'A' + i)
			count++;
	return (count / 2);
}

/* ---
#### void enigmaPlugboardMap(char a, char b)

Add a plugboard mapping between two characters.

**NOTE:** if either letter already had a mapping, its mapping is first removed before establishing the new mapping.
Mapping a letter to itself will clear that mapping.

--- */

void enigmaPlugboardMap(char a, char b) {
	int tmp, indexa, indexb;
	a = UPPERCASE(a);
	b = UPPERCASE(b); // upper case inputs
	indexa = CHAR2INDEX(a);
	indexb = CHAR2INDEX(b); // convert ASCII codes to zeroed indexes

	if (IS_VALID(indexa) && IS_VALID(indexb)) {
		_enigma_data_changed = true;

		// remove any existing connections which a and b might have
		if ((tmp = crypt(&plugboard, indexa)) != indexa) { plugboard.map[tmp] = INDEX2CHAR(tmp); }
		if ((tmp = crypt(&plugboard, indexb)) != indexb) { plugboard.map[tmp] = INDEX2CHAR(tmp); }

		// wire a to b and vice versa
		plugboard.map[indexa] = b;
		plugboard.map[indexb] = a;
	}
}

/* ---
#### void enigmaPlugboardClear()

Remove all mappings in the plugboard.

--- */
void enigmaPlugboardClear() {
	_enigma_data_changed = true;
	for (uint8_t i = 0; i < ENIGMA_MAP_SIZE; i++)
		plugboard.map[i] = 'A' + i;
}

/* ---
#### char enigmaPlugboardGet()

Return a character string representation of the plugboard where mapped letters occupy their mapped counterparts.

Example: a cleared plugboard would be represented by `ABCDEFGHIJKLMNOPQRSTUVWXYZ` and
a plugboard with a mapping between `G` and `R` would be represented by `ABCDEFRHIJKLMNOPQGSTUVWXYZ`.

--- */
char *enigmaPlugboardGet() {
	return plugboard.map;
}

const char *enigmaRotorsRead() {
	return rotation_chars;
}


/* ---
## Rotor Positions and Keys

**Positions:** A rotor position refers to the letter displayed by the rotor.
On a physical Enigma machine, each rotor has letters around a ring which face outward when viewing the rotor on-edge.
The operator may use a thumbwheel on each rotor to rotate it to a given position, described by the letter
visible through a window of the cover over the rotors. The API provides functions for setting and getting the rotor position using letters.

**Encryption Keys:** Normal operations require changing the rotors' starting positions.
For convenience, the API maintains three sets of positions which is calls **keys** _(to align with
their original purpose of being the keys used for encrypting and decrypting messages)_.

The entire rotor set may have their positions set by using a **key** string.
Likewise, the position of the entire rotor set may be obtained as a string.

The API has functions for the **daily** key, the **message** key, and the **encoded** key.

In historical usage, the **daily** key was used for a whole day and was provided on the daily sheet.
The **message** key was chosen at random or constructed from other information of the daily sheet or code books.
The **encoded** key is a convenience and is generated from the **message** key using the `enigmaEncode()` function.

When receiving a message, the message contents may include the **encoded** key and the API provides
a convenient function for reconstructing the *message** key.

**Notes:**

Messages were constructed with blocks of letters. These blocks were either 4 or 5 characters in size.
Since the keys The keys were transmitted with the message, the keys length needs to be align with the block size.

The string representation of a `key` is processed with the first character reprenting right most rotor _slot_.
Only a 4-rotor machine will use the first _slot_. On 3-rotor configurations, the first letter of the `key` is ignored.
Additionally, the `key` is padded on the right with zero or one letters to match the block size set from `enigmaKeySizeSet()`.

--- */

static void _enigma_rotor_set_starting_num(int position, int rotation) {
	if (position >= num_rotors) return;
	_enigma_data_changed = true;
	// wrap around on map
	if (rotation < 1)
		rotation = ENIGMA_MAP_SIZE;

	// input values are 1..26 so we need to switch to 0..25
	if (rotation > 0) rotation--;

	rotation = rotation % ENIGMA_MAP_SIZE;
	rack[position].rotation = rotation;
	rack[position].initial = rotation;
	rotation_chars[position] = 'A' + rack[position].rotation;
}
#if 0
uint8_t _enigma_rotor_get_starting_num(int position) {
	if (position >= num_rotors) return 0;
	// user values are 1..26 so we need to switch to 0..25
	return rack[position].rotation + 1;
}

void _enigma_reset_starting_position() {
	for (uint8_t i = 0; i < ENIGMA_RACK_SIZE; i++) {
		rack[i].rotation = rack[i].initial;
		rotation_chars[i] = 'A' + rack[i].rotation;
	}
}
#endif

/* ---
#### void enigmaRotorLetterSet(int position, char letter)

Rotate the rotor in the specific position to the letter.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).

_This function is equivelent to an operator of a physical Enigma machine
using the thumb wheel to change which letter of the rotor shows through the small window of the case._

--- */

void enigmaRotorLetterSet(int position, char letter) {
	// wrap around on map
	if (letter < 'A')		letter = 'A';
	if (letter > 'Z')		letter = 'Z';

	_enigma_rotor_set_starting_num(position, (letter - 'A') + 1);	// position is 1's based
}


/* ---
#### char enigmaRotorLetterGet(int position)

Return the letter of the rotor in the specific position.

The letters of the rotors will change with calls to `enigmaEncode()` and `enigmaEncodeString()`.
The right most rotor letter will change with each encoding and when it reaches a notch position
it will rotate the rotor to its left one position. The same process occurs between the middle rotor and the right rotor.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).

--- */

char enigmaRotorLetterGet(int position) {
	if (position >= num_rotors) return 0;
	// user values are 1..26 so we need to switch to 0..25
	return 'A' + rack[position].rotation;
}


/* ---
#### bool enigmaKeySizeSet(uint8_t size)

Set the block size used for rendering keys as strings.
Only a size of 4 or 5 is supported. Values outside this range are chagne to conform.

Returns `true` if the new block size is different from the previous block size.

_A common practice is for the key size to match the block size used when constructing encoded messages. This way, the keys are individual blocks._

--- */

bool enigmaKeySizeSet(uint8_t size) {
	if (size < 4) size = 4;
	if (size > 5) size = 5;
	if (block_size == size)
		return false;

	_enigma_data_changed = true;
	block_size = size;
	return true;
}

/* ---
#### uint8_t enigmaKeySizeGet()

Get the current block size used by `enigmaEncodeString()` and for rendering keys as strings.

_Enigma encrypted messages were often renders in blocks of 4 or 5 letters._

--- */

uint8_t enigmaKeySizeGet() {
	return block_size;
}


/* ---
#### void enigmaKeyMessageRestart()

Return the letter of the rotor in the specific position.

The letters of the rotors will change with calls to `enigmaEncode()` and `enigmaEncodeString()`.
The right most rotor letter will change with each encoding and when it reaches a notch position
it will rotate the rotor to its left one position. The same process occurs between the middle rotor and the right rotor.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).

--- */

void enigmaKeyMessageRestart() {
	memset(rotation_chars, 0, ENIGMA_RACK_SIZE + 1);
	// set the rotors positions back to the key
	for (uint8_t i = 0; i < ENIGMA_RACK_SIZE; i++) {
		enigmaRotorLetterSet((ENIGMA_RACK_SIZE - 1) - i, _message_key[i]);
	}
}

void enigmaKeyDailyRestart() {
	memset(rotation_chars, 0, ENIGMA_RACK_SIZE + 1);
	// set the rotors positions back to the key
	for (uint8_t i = 0; i < ENIGMA_RACK_SIZE; i++) {
		enigmaRotorLetterSet((ENIGMA_RACK_SIZE - 1) - i, _daily_key[i]);
	}
}



void _enigma_set_keys(char *key, bool encoded) {
	_enigma_data_changed = true;

	uint8_t len = enigmaKeySizeGet();
	memset(_message_key, 0, ENIGMA_KEY_SIZE+1);

	// temporarily set machine to daily key to the encode/decode message key
	enigmaKeyDailyRestart();

	// generate encoded key from message key
	for (uint8_t i = 0; i < len; i++) {
		char c = key[i];
		if (!ISLETTER(c))	c = randomCharGet();				// safety net in case the key is too short
		if (encoded)		_message_key[i] = enigmaEncode(c);	// decode character to get message key
		else				_message_key[i] = c;				// store message key
	}

	enigmaKeyMessageRestart();	// set machine key

	//printDevicePrintf(PRINT_UART, "esk(%s): %s %s [%s]\n", key, _daily_key, _message_key, rotation_chars);

	_using_random_key = false; // assume this key was manually set
}


/* ---
#### void enigmaKeyMessageSet(char* key)

Rotate the rotors in rotor set to match the letters of the key.

**NOTE:** The `key` is processed from left to right.
Displaying the `key` directly as a string may have leading or trailing letters not used.

_This function is equivelent to an operator of a physical Enigma machine
using the thumb wheel to change which letters of the rotors show through the small windows of the case._

--- */

void enigmaKeyMessageSet(char *key) {
	_enigma_set_keys(key, false);
}

/* ---
#### void enigmaKeyMessageRandomize()

Create a random message key and set the rotors using that key.

**Warning:** This function requires `randomCharGet()`. If this function is not defined, a brain dead macro
provides a non random character.

--- */

void enigmaKeyMessageRandomize() {
	char temp[ENIGMA_KEY_SIZE+1];
	uint8_t len = enigmaKeySizeGet();

	for (uint8_t i = 0; i < len; i++)
		temp[i] = randomCharGet();
	temp[len] = 0;

	enigmaKeyMessageSet(temp);
	_using_random_key = true;	// system is now using a random key
}


/* ---
#### void enigmaKeyMessageRandomizeConditionally()

If the current message key was randomly generated, then create a new random message key
If the current message key was set directly using `enigmaKeyMessageSet(), then this function will perform `enigmaKeyMessageRestart()` and the current message key is preserved.

_This is a convenience function._

--- */
void enigmaKeyMessageRandomizeConditionally() {
	// generate a new random message key if the current key was also a random key
	if (_using_random_key)
		enigmaKeyMessageRandomize();
	else
		enigmaKeyMessageRestart();
}


/* ---
#### char* enigmaKeyMessageGet()

Return the current message key as a string.

--- */
char *enigmaKeyMessageGet() {
	return _message_key;
}


/* ---
#### void enigmaKeyMessageFromEncoded(char* key)

Set the message key from it's encoded form

Compute the Message key given the encoded form; uses the Daily key to perform the decoding.
Rotate the rotors in rotor set to match the letters of the resulting message key.

**Warning:** This function used the `enigmaEncode()` function and requires the API previously be
configured with the **machine**, **rotors**, **rings**, and **plugboard**. Additionally, the **daily** key must already be present.

--- */
void enigmaKeyMessageFromEncoded(char *key) {
	_enigma_set_keys(key, true);
}


/* ---
#### void enigmaKeyMessageToEncoded(char *buffer)

Return the encoded form of the message key.

The key is returned in the buffer provided. The buffer must be at least (ENIGMA_KEY_SIZE + 1) in size.

**Warning:** This function used the `enigmaEncode()` function and requires the API previously be
configured with the **machine**, **rotors**, **rings**, and **plugboard**. Additionally, the **daily** key must already be present.

--- */
void enigmaKeyMessageToEncoded(char *buffer) {
	uint8_t len = enigmaKeySizeGet();

	memset(buffer, 0, (ENIGMA_KEY_SIZE + 1));

	// temporarily set the rotors to the daily key to encode the message key
	enigmaKeyDailyRestart();

	for (uint8_t i = 0; i < len; i++)
		buffer[i] = enigmaEncode(_message_key[i]);
	enigmaKeyMessageRestart();
}


/* ---
#### void enigmaKeyDailySet(char* key)

Store the new daily key and generate a new random message key (and encoded key).

--- */

void enigmaKeyDailySet(char *key) {
	_enigma_data_changed = true;

	memset(_daily_key, 'A', ENIGMA_KEY_SIZE);
	memset(_message_key, 'A', ENIGMA_KEY_SIZE);

	if (enigmaKeySizeGet() == 5) {
		_daily_key[ENIGMA_KEY_SIZE] = 0;
		_message_key[ENIGMA_KEY_SIZE] = 0;
	} else {
		_daily_key[ENIGMA_KEY_SIZE-1] = 0;
		_message_key[ENIGMA_KEY_SIZE-1] = 0;
	}

	if (key) {
		uint8_t len = strlen(key);
		if (len > enigmaKeySizeGet()) len = enigmaKeySizeGet();

		for (uint8_t i = 0; i < len; i++)
			_daily_key[i] = key[i];
	}

	enigmaKeyMessageRandomize();
}

/* ---
#### char* enigmaKeyDailyGet()

Return the current daily key as a string.

--- */

char *enigmaKeyDailyGet() {
	return _daily_key;
}


/* ---
#### void enigmaKeyResize()

Adjust the key length to match the current block size.

**Note:** This function does not alter the significant data of the keys. It only adjust the number of padding characters.

--- */

void enigmaKeyResize() {
	uint8_t new_len = enigmaKeySizeGet();
	uint8_t old_len = strlen(_daily_key);

	if (new_len == old_len) return;

	char temp[ENIGMA_KEY_SIZE + 1];
	memset(temp, 0, ENIGMA_KEY_SIZE + 1);

	if (new_len < old_len) {
		_daily_key[4] = 0;
		_message_key[4] = 0;
	} else {
		_daily_key[4] = randomCharGet();
		_message_key[4] = randomCharGet();
	}

	memcpy(temp, _message_key, ENIGMA_KEY_SIZE);
	enigmaKeyMessageSet(temp);
}




/* ---
## Machine Configuration Functions
--- */

/* ---
#### void enigmaReflectorSet(int reflector)

Set the reflector type.

There are two possible reflectors for each machine type - **B** and **C**.
On a physical M4 Enigma machine, these rotors were thinner to fit within the same space  and allow room for adding the **Beta** or **Gamma** rotor to the rotor set.

_For convenience, this code will map between _regular_ and _thin_ reflectors._

--- */

void enigmaReflectorSet(int ref) {
	_enigma_data_changed = true;

	// wrap reflectors in both directions
	if (ref < REFLECTOR_B) ref = REFLECTOR_C_THIN;
	if (ref > REFLECTOR_C_THIN) ref = REFLECTOR_B;

	if ((machine_type == MACHINE_M4) && (ref < REFLECTOR_B_THIN))
		ref += REFLECTOR_B_THIN;
	if ((machine_type < MACHINE_M4) && (ref > REFLECTOR_C))
		ref -= REFLECTOR_B_THIN;

	reflector = reflectors[ref];

	// update the machine names to include the new reflector type
	char c = 'B';
	if ((reflector.code == REFLECTOR_C) || (reflector.code == REFLECTOR_C_THIN))
		c = 'C';
	// update ALL machine names
	for (uint8_t i = 0; i < MACHINES_SIZE; i++) {
		uint8_t len = strlen(machine_names[i]);
		if (len)
			machine_names[i][len - 1] = c;
	}
}

/* ---
#### void enigmaReflectorToggle()

A convenience function to toggle between **B** and **C** or **Thin B** and **Thin C**.

--- */
void enigmaReflectorToggle() {
	enigmaReflectorSet(reflector.code + 1);
}

/* ---
#### uint8_t enigmaReflectorGet()

Return the enumerated value of the reflector.

--- */
uint8_t enigmaReflectorGet() {
	return reflector.code;
}


/* ---
#### void enigmaMachineSetRaw(uint8_t machine)

Set the API to use the enumerated machine type.

This function does not reset the reflector or rotor set. Once those additional functions are called,
use `enigmaMachineValidate()` to insure the combined settings are valid.

_This function is provided to allow a user interface to change multiple settings prior to validation._

--- */

void enigmaMachineSetRaw(uint8_t m) {
	if (m > MACHINE_M4)
		m = 0; // default to simplest machine

	if (m == machine_type) return;
	_enigma_data_changed = true;

	machine_type = m;

	//printDevicePrintf(PRINT_UART, "Machine = %s\n", machine_names[machine_type]);

	num_options = 8; // assume 8 available rotors (used by navy)

	switch (machine_type) {
		case MACHINE_W3: {
			num_options = 5; // non navy machines only have 5 possible rotors
		}					 // fall through
		case MACHINE_M3: {
			num_rotors = 3;
			//enigmaReflectorSet(REFLECTOR_B);
		} break;
		case MACHINE_M4: {
			num_rotors = 4;
			//enigmaRotorLoad(ROTOR_EXTRA, ROTOR_BETA);
			//enigmaReflectorSet(REFLECTOR_B_THIN);
		} break;
	}
	// WARNING: must run enigmaMachineValidate() before starting to use the machine
}


/* ---
#### void enigmaMachineSet(uint8_t machine)

Set the API to use the enumerated machine type.

This function performs validation of all machine settings (rotors, reflector, etc).

--- */

// forward declaration to improve documentation
void enigmaMachineValidate();

void enigmaMachineSet(uint8_t m) {
	enigmaMachineSetRaw(m);
	enigmaMachineValidate();
}

/* ---
#### void enigmaMachineValidate()

Perform validation of the API settings and adjust as necessary to conform to any machine limits and eliminate any conflicting settings.

--- */
void enigmaMachineValidate() {
	// insure the current machine type has valid rotors and reflector
	uint8_t r;
	for (uint8_t i = 0; i < ENIGMA_RACK_SIZE; i++) {
		r = enigmaRotorNumGet(i);
		// make sure rotor is valid
		enigmaRotorLoadAvailable(i, r);
	}
	enigmaReflectorSet(enigmaReflectorGet());

	// need to recompute the keys because the rotors or reflector may have changed
	char temp[ENIGMA_KEY_SIZE + 1];
	memcpy(temp, _message_key, ENIGMA_KEY_SIZE + 1);
	enigmaKeyMessageSet(temp);
}


/* ---
#### uint8_t enigmaMachineNumGet()

return the enumerated value for the current machine type.

--- */
uint8_t enigmaMachineNumGet() {
	return machine_type;
}

/* ---
#### const char* enigmaMachineNameGet()

return the string name of the current machine type.

--- */
const char *enigmaMachineNameGet() {
	return machine_names[machine_type];
}

/* ---
#### const char** enigmaMachineNamesGet()

return and array of strings reprenting the names of the available machine types.

_This is a convenience function for developing user interfaces._

--- */
char **enigmaMachineNamesGet() {
	return machine_names;
}

/* ---
#### uint8_t enigmaMachineRotorsetGet()

return the number of rotors used in the machine.

--- */
uint8_t enigmaMachineRotorsetGet() {
	//printDevicePrintf(PRINT_UART, "Rotors = %d\n", num_rotors);
	return num_rotors;
}

/* ---
#### uint8_t enigmaMachineRotorAvailable()

return the number of available rotors to select from when loading rotors.

--- */
uint8_t enigmaMachineRotorsAvailable() {
	//printDevicePrintf(PRINT_UART, "Rotor Set = %d\n", num_options);
	return num_options;
}


void enigmaMachineDump() {
	printDevicePrintf(PRINT_UART, "%s (ref:%d rts: %d)\n", enigmaMachineNameGet(), enigmaReflectorGet(), enigmaMachineRotorsetGet());
	for (uint8_t i = 0; i < ENIGMA_RACK_SIZE; i++) {
		printDevicePrintf(PRINT_UART, "%d: %4s %d %c\n", i, enigmaRotorNameGet(i), enigmaRotorRingGet(i), enigmaRotorLetterGet(i));
	}
	printDevicePrintf(PRINT_UART, "DME: %s %s [%s]\n", enigmaKeyDailyGet(), enigmaKeyMessageGet(), enigmaRotorsRead());
	uartPutChar('\n');
}

/* ---
## API Functions
--- */


/* ---
#### void enigmaInit(uint8_t default_machine)

This function must be called before using any other functions from the API.

It sets the the API to the specified machine type and default values for all other settings.

--- */

void enigmaInit(uint8_t machine) {
	enigmaChangedClear();

	block_size = 4;
	enigmaMachineSetRaw(machine);
	enigmaRotorLoad(ROTOR_LEFT, ROTOR_I);
	enigmaRotorLoad(ROTOR_MIDDLE, ROTOR_II);
	enigmaRotorLoad(ROTOR_RIGHT, ROTOR_III);
	enigmaRotorLoad(ROTOR_EXTRA, ROTOR_BETA);
	enigmaMachineValidate();
	enigmaPlugboardClear();
	enigmaKeyDailySet(NULL);
}

#endif	// __ENIGMA_API_H_
