
# Enigma Encryption and Configuration API

The origins of this code flow back to [munnellg EnigmaMachine](https://github.com/munnellg/EnigmaMachine) project under the MIT license.

The original author stated, _"If you're planning on using this in a project of your own, bear in mind that it has had very little testing. It's a quick little hack I threw together to pass the time on a rainy weekend."_

The following API has been refactored, filled out with Get/Set functions, and extended to support actual enigma machine tasks beyond basic encrypt/decript of characters.

**Note:** The API documentation order has been chosen to avoid as much forward referencing as possible.
While this is less intuitive for the reader, it is more conducive to the maintenance of the code. _it is what it is_

--------------------------------------------------------------------------

## Enigma Machine

"The Enigma machine is a cipher device developed and used in the early- to mid-20th century to protect commercial, diplomatic, and military communication. It was employed extensively by Nazi Germany during World War II, in all branches of the German military." - [WIKIPEDIA](https://en.wikipedia.org/wiki/Enigma_machine)

There has been plenty of descriptions of the Enigma machine. It was not just an encryption device used by the Germans during WWII.
However, the usecase driving the development and test of this API is to support the implementation of a digital facsimile if the military
hardware. Civilian devices may be implemented using this API provided the desired machine used rotors which were used by one of the
machine types implemented within the API.

If you are interesting is experiencing an Enigma machine, consider the [Enigma Simulator](http://users.telenet.be/d.rijmenants/en/enigmasim.htm)
which provides an accurate visual and functional representation as well as considerable documentation and tutorials.


### Machine

The German military Enigma machines had either 3 or 4 positions and the machines came with either a set of 5 rotors, 8 rotors, or 8+2 rotors.

There are three machine types supported by this project.

|Machine|Rotor<br>positions|Available<br>Rotors|Reflector|Plug Wires|
|:-----|:-----:|:-----:|:-----:|:-----:|
|Wehrmacht|3|5|B or C|10|
|Kriegsmarine M3|3|8|B or C|10|
|Kriegsmarine M4|4|8<br>Beta<br>Gamma|Thin<br>B or C|10|

While the API provides configuration for the machine type, the physical machine _is what it is_ and does not change.


### Reflector

A physical Enigma machine has a pre-stalled reflector disk which has internal mapping wires and _reflects_ the electrical signal back through the rotors.

While the API provides configuration for the machine type and the reflector, the physical machine _is what it is_ and does not change.

### Rotor

A rotor from a real Enigma machine has a specifc number stamped on it.
Rotors were indicated with Roman numerals.
It has thumb wheel on one side and a wide rotating ring on the other.
The ring is help in place by a spring clip. Pressing the spring clip allows the ring to rotate independent of the rest of the rotor.
The ring has numbers on the side and letters around the perimeter.

When a rotor is held vertically, the letters are visible.
When a rotor is help face on with the thumb wheel at the back, the ring numbers are visible.


### Plugboard

The lower front of a military Enigma machine consisted of 26 plugs.
The machine was provided with 10 wires with compatible plugs on each end.
The plug wires perform letter substitution.
For example, assumed there is a wire between plug 'A' and 'R' and another between 'H' and 'B'.
When a user hit the 'R' key it would behave like an 'A' at the beginning of the encrypting sequenced.
If the end of the encrypting sequence resulted in a 'B' it would light up the 'H' bulb as the final step.
The wires were reciprocal, meaning 'R' becomes 'A' and 'A' become 'R'. Likewise, 'B' becomes 'H' and 'H' becomes 'B'.

### Operations

The German Enigma machines has multiple settings. The procedure for determining the settings varied for different services and at different times during WWII.

As a generalization, the 3 or 4 rotors were selected from the available set.
Each rotor had its ring positions set. Then the rotors were inserted into their designated position.
Additionally, plugboard wires were installed.

Daily settings sheets were used to determine which rotors to use,
what ring settings to use, what position order for the rotors, and what letters of the plugboard were populated.

The operator also needed to choose a code key. Depending on what procedure was used, the key was determined - in part - from the daily sheet or a code book.


## API Definitions and Enumerations

```C
*/
// operational modes
enum { MACHINE_W3 = 0,
	   MACHINE_M3,
	   MACHINE_M4
};
/*
```

```C
*/
// encryption rotors to be placed in the rack.
// ROTOR_BETA and ROTOR_GAMMA can only go in the
// fourth rack position and are only used with MACHINE_M4
enum { ROTOR_I = 0,
	   ROTOR_II,
	   ROTOR_III,
	   ROTOR_IV,
	   ROTOR_V,
	   ROTOR_VI,
	   ROTOR_VII,
	   ROTOR_VIII,
	   ROTOR_BETA,
	   ROTOR_GAMMA };
/*
```

```C
*/
// The position within the rotor set is numerically ordered from right to left
enum { ROTOR_RIGHT = 0,
	   ROTOR_MIDDLE,
	   ROTOR_LEFT,
	   ROTOR_EXTRA
};
/*
```

```C
*/
// reflectors which sit at the end of the rack and bounce signal back through rotors.
// A physical MACHINE_M4 Enigma machine uses thin reflectors. Each reflector has a different internal mapping.
enum { REFLECTOR_B = 0,
	   REFLECTOR_C,
	   REFLECTOR_B_THIN,
	   REFLECTOR_C_THIN
};
/*
```

## Operational Functions

**Encode / Decode**: The primary operations used by a program will be the `enigmaEncode()` and `EnigmaEncodeString()`
functions.


#### void enigmaRotorsBackspin()

Reverse the active rotor set one position

The active rotor set is automatically advanced during the `enigmaEncode()` process.
The only reason to use this function is as part of a user interface which supports
a `DEL` key to delete the most recent character(s).

_Implementing a general purpose editor with cursor navigation and simultaneous encoded would be very complex._


#### char enigmaEncode(char letter)

Return the encrypted letter for the input letter.

This function will advance the active rotor set one position.


#### uint8_t enigmaEncodeString(char* input, char* output, uint8_t size)

Encode the string and return the number of characters encoded.

This function will only encrypt ASCII letters. Letters are converted to upper case. non-letters are ignored.

This function uses the current value from `enigmaKeySizeGet()` to determine the blocksize.

The output is padded with characters to a full blocksize.
If padding will exceed the `output` buffer `size`, then the `output` is truncated to the last full blocksize.

This function will set the rotors to their start position before performing the encoding process.
Encoding will advance the active rotor set for each character encoded. The rotor set are not advanced for spaces.


## Rotor Configuration Functions

The historical process of setting up the rotors for the Enigma machine was to
- identify the rotor set _(rotors to use from all available rotors)_
- order the rotor set
- adjust the ring position for each rotor
- insert the rotor set into the machine

The plugboard may be setup before or after the rotors.

_Whereas the full rotor set was inserted in an Enigma machine, the API loads one rotor at a time into the machine._


#### void enigmaRotorLoad(int position, uint8_t rotor)

Set the rotor position to the specific rotor.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The rotor is one-based. To avoid confusion, it is recommended to use _(or at least reference)_ the enumerated rotor indentifiers.

**WARNING:** This function does not test for collisions. Use `enigmaRotorLoadAdjust()` or `enigmaRotorLoadAdjust()` to automatically avoid conflicts.


#### void enigmaRotorLoadAdjust(int position, uint8_t rotor)

Set the rotor position to the specific rotor. Adjust other rotors to avoid conflicts.
If setting the current rotor will conflict with another rotor, then the conflicting rotor is changed to resolve the conflict.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The rotor is one-based. To avoid confusion, it is recommended to use _(or at least reference)_ the enumerated rotor indentifiers.


#### void enigmaRotorLoadAvailable(int position, uint8_t rotor)

Attempt to set the rotor position to the specific rotor.
If this will cause a conflict, then change the specified rotor to an unused rotor.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The rotor is one-based. To avoid confusion, it is recommended to use _(or at least reference)_ the enumerated rotor indentifiers.


#### uint8_t enigmaRotorNumGet(int position)

Return the enumerated value of the rotor in the specific position.
_(refer to the [enumerations](#api-definitions-and-enumerations) near the start of this documentation)_

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).


#### char* enigmaRotorNameGet(int position)

Return string representing the _Roman numeral_ ror the rotor in the specific position.
Rotors were indicated with .

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).

_This is a convenience function for developing user interfaces._


#### void enigmaRotorRingSet(int position, uint8_t value)

Adjust the ring value of rotor in the specific position.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The ring value is one-based (1 .. 26).


#### uint8_t enigmaRotorRingGet(int position)

Return the current ring value of rotor in the specific position.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).
The ring value is one-based (1 .. 26).


#### void enigmaPlugboardMap(char a, char b)

Returns the number of plug wires currently applied

The upper limit is 13.

_Original machines had up to 10 wires because there is little improvement in complexity with 11 and beyond 11 the complexity actually goes down._


#### void enigmaPlugboardMap(char a, char b)

Add a plugboard mapping between two characters.

**NOTE:** if either letter already had a mapping, its mapping is first removed before establishing the new mapping.
Mapping a letter to itself will clear that mapping.


#### void enigmaPlugboardClear()

Remove all mappings in the plugboard.


#### char enigmaPlugboardGet()

Return a character string representation of the plugboard where mapped letters occupy their mapped counterparts.

Example: a cleared plugboard would be represented by `ABCDEFGHIJKLMNOPQRSTUVWXYZ` and
a plugboard with a mapping between `G` and `R` would be represented by `ABCDEFRHIJKLMNOPQGSTUVWXYZ`.


## Rotor Positions and Keys

**Positions:** A rotor position refers to the letter displayed by the rotor.
On a physical Enigma machine, each rotor has letters around a ring which face outward when viewing the rotor on-edge.
The operator may use a thumbwheel on each rotor to rotate it to a given position, described by the letter
visible through a window of the cover over the rotors. The API provides functions for setting and getting the rotor position using letters.

**Encryption Keys:** Normal operations require changing the rotors' starting positions.
For convenience, the API maintains three sets of positions which is calls **keys** _(to align with
their original purpose of being the keys used for encrypting and decrypting messages)_.

The entire rotor set may have their positions set by using a **key** string.
Likewise, the position of the entire rotor set may be obtained as a string.

The API has functions for the **daily** key, the **message** key, and the **encoded** key.

In historical usage, the **daily** key was used for a whole day and was provided on the daily sheet.
The **message** key was chosen at random or constructed from other information of the daily sheet or code books.
The **encoded** key is a convenience and is generated from the **message** key using the `enigmaEncode()` function.

When receiving a message, the message contents may include the **encoded** key and the API provides
a convenient function for reconstructing the *message** key.

**Notes:**

Messages were constructed with blocks of letters. These blocks were either 4 or 5 characters in size.
Since the keys The keys were transmitted with the message, the keys length needs to be align with the block size.

The string representation of a `key` is processed with the first character reprenting right most rotor _slot_.
Only a 4-rotor machine will use the first _slot_. On 3-rotor configurations, the first letter of the `key` is ignored.
Additionally, the `key` is padded on the right with zero or one letters to match the block size set from `enigmaKeySizeSet()`.


#### void enigmaRotorLetterSet(int position, char letter)

Rotate the rotor in the specific position to the letter.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).

_This function is equivelent to an operator of a physical Enigma machine
using the thumb wheel to change which letter of the rotor shows through the small window of the case._


#### char enigmaRotorLetterGet(int position)

Return the letter of the rotor in the specific position.

The letters of the rotors will change with calls to `enigmaEncode()` and `enigmaEncodeString()`.
The right most rotor letter will change with each encoding and when it reaches a notch position
it will rotate the rotor to its left one position. The same process occurs between the middle rotor and the right rotor.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).


#### bool enigmaKeySizeSet(uint8_t size)

Set the block size used for rendering keys as strings.
Only a size of 4 or 5 is supported. Values outside this range are chagne to conform.

Returns `true` if the new block size is different from the previous block size.

_A common practice is for the key size to match the block size used when constructing encoded messages. This way, the keys are individual blocks._


#### uint8_t enigmaKeySizeGet()

Get the current block size used by `enigmaEncodeString()` and for rendering keys as strings.

_Enigma encrypted messages were often renders in blocks of 4 or 5 letters._


#### void enigmaKeyMessageRestart()

Return the letter of the rotor in the specific position.

The letters of the rotors will change with calls to `enigmaEncode()` and `enigmaEncodeString()`.
The right most rotor letter will change with each encoding and when it reaches a notch position
it will rotate the rotor to its left one position. The same process occurs between the middle rotor and the right rotor.

**NOTE:** The `position` is zero-based with the left most position being zero (`0`).


#### void enigmaKeyMessageSet(char* key)

Rotate the rotors in rotor set to match the letters of the key.

**NOTE:** The `key` is processed from left to right.
Displaying the `key` directly as a string may have leading or trailing letters not used.

_This function is equivelent to an operator of a physical Enigma machine
using the thumb wheel to change which letters of the rotors show through the small windows of the case._


#### void enigmaKeyMessageRandomize()

Create a random message key and set the rotors using that key.

**Warning:** This function requires `randomCharGet()`. If this function is not defined, a brain dead macro
provides a non random character.


#### void enigmaKeyMessageRandomizeConditionally()

If the current message key was randomly generated, then create a new random message key
If the current message key was set directly using `enigmaKeyMessageSet(), then this function will perform `enigmaKeyMessageRestart()` and the current message key is preserved.

_This is a convenience function._


#### char* enigmaKeyMessageGet()

Return the current message key as a string.


#### void enigmaKeyMessageFromEncoded(char* key)

Set the message key from it's encoded form

Compute the Message key given the encoded form; uses the Daily key to perform the decoding.
Rotate the rotors in rotor set to match the letters of the resulting message key.

**Warning:** This function used the `enigmaEncode()` function and requires the API previously be
configured with the **machine**, **rotors**, **rings**, and **plugboard**. Additionally, the **daily** key must already be present.


#### void enigmaKeyMessageToEncoded(char *buffer)

Return the encoded form of the message key.

The key is returned in the buffer provided. The buffer must be at least (ENIGMA_KEY_SIZE + 1) in size.

**Warning:** This function used the `enigmaEncode()` function and requires the API previously be
configured with the **machine**, **rotors**, **rings**, and **plugboard**. Additionally, the **daily** key must already be present.


#### void enigmaKeyDailySet(char* key)

Store the new daily key and generate a new random message key (and encoded key).


#### char* enigmaKeyDailyGet()

Return the current daily key as a string.


#### void enigmaKeyResize()

Adjust the key length to match the current block size.

**Note:** This function does not alter the significant data of the keys. It only adjust the number of padding characters.


## Machine Configuration Functions

#### void enigmaReflectorSet(int reflector)

Set the reflector type.

There are two possible reflectors for each machine type - **B** and **C**.
On a physical M4 Enigma machine, these rotors were thinner to fit within the same space  and allow room for adding the **Beta** or **Gamma** rotor to the rotor set.

_For convenience, this code will map between _regular_ and _thin_ reflectors._


#### void enigmaReflectorToggle()

A convenience function to toggle between **B** and **C** or **Thin B** and **Thin C**.


#### uint8_t enigmaReflectorGet()

Return the enumerated value of the reflector.


#### void enigmaMachineSetRaw(uint8_t machine)

Set the API to use the enumerated machine type.

This function does not reset the reflector or rotor set. Once those additional functions are called,
use `enigmaMachineValidate()` to insure the combined settings are valid.

_This function is provided to allow a user interface to change multiple settings prior to validation._


#### void enigmaMachineSet(uint8_t machine)

Set the API to use the enumerated machine type.

This function performs validation of all machine settings (rotors, reflector, etc).


#### void enigmaMachineValidate()

Perform validation of the API settings and adjust as necessary to conform to any machine limits and eliminate any conflicting settings.


#### uint8_t enigmaMachineNumGet()

return the enumerated value for the current machine type.


#### const char* enigmaMachineNameGet()

return the string name of the current machine type.


#### const char** enigmaMachineNamesGet()

return and array of strings reprenting the names of the available machine types.

_This is a convenience function for developing user interfaces._


#### uint8_t enigmaMachineRotorsetGet()

return the number of rotors used in the machine.


#### uint8_t enigmaMachineRotorAvailable()

return the number of available rotors to select from when loading rotors.


## API Functions

#### void enigmaInit(uint8_t default_machine)

This function must be called before using any other functions from the API.

It sets the the API to the specified machine type and default values for all other settings.


